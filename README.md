# Project for challenge 1 of 305cde (Developing the modern Web 2)

## Overview
The calculator is an application that can be used to evaluate any computation containing the 4 basic operations + - * / and parantheses.

Multiple calculations can be enqueued in order to eventually be evaluated. A calculation must be complete in order to be enqueued, which can be achieved by the _Q_-button or by pressing the _Q_-key.

The calculatioin can either be input by using the buttons or by pressing valid keyboard keys. To remove the last character of the calculation, use the _Backspace_-key or the _Remove_-button and to clear the calculation and all enqueued calculations, use the _Escape_-key or the _Clear_-button.

## How it works
The user is unable to input an invalid calculation since all invalid keys are being ignored and all invalid buttons are disabled. To judge which characters are invalid, the current, partially input calculation is being evaluated.

Solving all calculations is done by applying the [Shunting Yard Algorithm](http://en.wikipedia.org/wiki/Shunting-yard_algorithm) in order to convert the calculation from it's initial form into [Reverse Polish Notation](http://en.wikipedia.org/wiki/Reverse_Polish_notation) which then can be evaluated fairly easily.

Two kinds of key-events are being used, _keypress_ for processing _normal_ key events where the evaluated key is required (eg. pressing _Shift + 8_ results in the evaluated key _*_ on an english keyboard layout). _keydown_ events are required for several _special_ key events where original behavior from the browser is intercepted. Specifically, _Backspace_-, _Escape_-, _Left Arrow_-, and _Up Arrow_-events are being intercepted.

## Known issues
- it is possible to enter calculations which divide by zero eg. 1/(1-1)

## Reflection
In my opinion, this was a good, difficult first challenge where I could familiarize myself with JavaScript since I did not have any previous experience with JavaScript or HTML. To use Reverse Polish Notation in order to avoid _eval()_ was an easy decision. The challenging part was to convert the standard form of the calculation into RPN which was best done with the Shunting Yard Algorithm, as Google searches showed.

The evaluation of calculations can easily be extended to allow more operators, eg. bitwise operators. Merely two functions need to be adjusted, _canPushOperator(...)_ which is required for operator priority, and _evaluate(...)_ which does the actual computation.

One of the big issues was dealing with the key events which was important to me since I don't believe in using the mouse. Since there are several different key events initially it was a process of trial-and-error to determine which events could be used for which scenario.

In order to prevent the user from editing any part of the calculation other than the tail, the _setCursor()_ function is used. It's original form can be found on [stackoverflow](http://stackoverflow.com/questions/1865563/set-cursor-at-a-length-of-14-onfocus-of-a-textbox/1867393#1867393), mine includes slight adjustments since I don't need to set the cursor to a specific position, only to the end of the calculation.

Regular expressions have been used to obtain the number of left and right brackets in the calculation.

Since the calculator functions are enclosed in a closure to reduce the pollution of the global space and I preferred to _add_ event handlers rather than _overwrite_ (possibly existing) event handlers, all buttons had to be retrieved and stored and the respective event handlers had to be added in the JavaScript code. It is debatable whether that was a good decision, I personally think that it probably wasn't.