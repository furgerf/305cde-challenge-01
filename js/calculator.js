/*global alert*/
var calculator = (function () {
    'use strict';

    ///////////////
    // VARIABLES //
    ///////////////

    var calculation = document.getElementById('calculation'),
        buttons,
        clearInput,
        setCursor = function () {
            // ensures that the cursor is set to the last position
            // function found here: http://stackoverflow.com/questions/1865563/set-cursor-at-a-length-of-14-onfocus-of-a-textbox/1867393#1867393

            var node = calculation,
                pos = calculation.value.length,
                textRange;

            node = (typeof node === "string" || node instanceof String) ? document.getElementById(node) : node;

            if (!node) {
                return false;
            } else if (node.createTextRange) {
                textRange = node.createTextRange();
                textRange.collapse(true);
                textRange.moveEnd(pos);
                textRange.moveStart(pos);
                textRange.select();
                return true;
            } else if (node.setSelectionRange) {
                node.setSelectionRange(pos, pos);
                return true;
            }

            return false;
        },
        removeChar = function () {
            // removes the last char

            if (calculation.value.length === 0) {
                return;
            }

            if (calculation.value.indexOf("=") > -1) {
                clearInput();
                return;
            }

            calculation.value = calculation.value.substring(0, calculation.value.length - 1);

            buttons.setButtonsEnabled();
        },
        canPushOperator = function (newOperator, oldOperator) {
            // determines whether the new operator can be pushed onto the stack
            // the shunting yard algorithm says that an operator can be pushed onto the stack if it has 
            // HIGHER priority than the topmost operator on the stack

            return newOperator === "(" || oldOperator === "(" || ((newOperator === "*" || newOperator === "/") && (oldOperator === "+" || oldOperator === "-"));
        },
        evaluate = function (b, a, operator) {
            // evaluate an arithmetic operation
            // swap a and b because they are fed in reverse order
            // (because they're popped from the stack)

            if (operator === "+") {
                return a + b;
            }
            if (operator === "-") {
                return a - b;
            }
            if (operator === "*") {
                return a * b;
            }
            if (operator === "/") {
                return a / b;
            }

            alert('Unknown operator: "' + operator + '"');
            return NaN;
        },
        solveCalculation = function (string) {
            // evaluates a calculation supplied in string format

            // initialize variables
            var output = [], // string-array with data, first in RPN, then containing the result
                stack = [], // secondary stack to help conversion to RPN, then to evaluate RPN
                parsingNumber = false; // used to properly process multi-digit numbers

            // traverse calculation string
            string.split('').forEach(function (item) {
                if (!isNaN(item)) {
                    // parse number
                    if (parsingNumber) {
                        // multi-digit number: add digit to last number
                        output[output.length - 1] = parseInt(output[output.length - 1], 10) * 10 + parseInt(item, 10);
                        //alert(output);
                    } else {
                        // new number: add new entry to output
                        output.push(item);
                        parsingNumber = true;
                    }
                } else {
                    // parse operator
                    parsingNumber = false;

                    if (item === ")") {
                        //pop stack onto output until left bracket is found
                        while (stack[stack.length - 1] !== "(") {
                            output.push(stack.pop());
                        }
                        stack.pop();
                    } else {
                        // check stack add to stack if it has higher priority than last stack element
                        if (stack.length === 0 || canPushOperator(item, stack[stack.length - 1])) {
                            // if we're allowed to simply push the operator on the stack, do that
                            stack.push(item);
                        } else {
                            // pop stack until we can push the new operator or the stack is empty
                            while (stack.length > 0 && !canPushOperator(item, stack[stack.length - 1])) {
                                output.push(stack.pop());
                            }
                            stack.push(item);
                        }
                    }

                }
            });

            // empty stack onto output
            while (stack.length > 0) {
                output.push(stack.pop());
            }

            // OUTPUT NOW CONTAINS CALCULATION IN (hopefully correct) REVERSE POLISH NOTATION

            // calculate...
            output.forEach(function (item) {
                if (!isNaN(item)) {
                    // if element is a number it goes onto the stack
                    stack.push(item);
                } else {
                    // if element is an operator, pop the stack twice for the arguments and push the 
                    // result of the operation back onto the stack
                    stack.push(evaluate(parseInt(stack.pop(), 10), parseInt(stack.pop(), 10), item));
                }
            });

            // return result :)
            return stack.join();
        },
        calculate = (function () {
            // contains functions related to the calculation (-queue)

            var queue = document.getElementById('calculationQueue'),
                calculationQueue = [];

            return {
                calculate: function () {
                    // calculates all open calculations (current one and any queued ones)

                    // solve current calculation
                    calculation.value += " = " + solveCalculation(calculation.value);

                    if (calculationQueue.length !== 0) {
                        queue.innerHTML = "Queue:";
                        // solve all queued calculations
                        calculationQueue.forEach(function (item) {
                            item += " = " + solveCalculation(item);
                            queue.innerHTML += "<br/>" + item;
                        });

                        calculationQueue = [];
                    }

                    buttons.setButtonsEnabled();
                },
                enqueueCalculation: function () {
                    // adds the current calculation to the queue

                    if (queue.innerHTML === "") {
                        queue.innerHTML = "Queue:";
                    }

                    queue.innerHTML += "<br/>" + calculation.value + " = ?";
                    calculationQueue.push(calculation.value);
                    calculation.value = "";
                    
                    buttons.setButtonsEnabled();
                },
                clearQueue: function () {
                    queue.innerHTML = "";
                    calculationQueue = [];
                }
            };
        }()),
        encloseBrackets = function () {
            // encloses the current calculation with brackets

            calculation.value = "(" + calculation.value + ")";

            buttons.setButtonsEnabled();
        },
        buttonClick = function (e) {
            // called whenever a button is clicked. adds the button's text to the calculation string
            // is there a complete calculation? if so, clear it (as well as the queue)

            if (calculation.value.indexOf("=") > -1) {
                clearInput();
            }

            if (e.value === "=") {
                calculate.calculate();
            } else {
                calculation.value += e.value;
            }

            return buttons.setButtonsEnabled();
        },
        processKeyPress = (function () {
            // handle key press events

            // key map
            var keyCodeToButtonId = {
                40: "leftBracket",
                41: "rightBracket",
                // no key for enclosing brackets!
                113: "enqueue",

                43: "plus",
                45: "minus",
                42: "times",
                47: "division",
                13: "equals",
                61: "equals",


                49: "one",
                50: "two",
                51: "three",
                52: "four",
                53: "five",

                54: "six",
                55: "seven",
                56: "eight",
                57: "nine",
                48: "zero"

                // no key for backspace (remove) since it is handled in onKeyDown
                // no key for escape (clear) since it is handled in onKeyDown
            };

            return function (event) {
                // is there a complete calculation? if so, clear it
                if (calculation.value.indexOf("=") > -1) {
                    calculation.value = "";
                    document.getElementById('calculationQueue').innerHTML = "";
                }

                // prevent addition of char (if the char is valid, it'll be added later)
                event.preventDefault();

                // check whether key is valid
                if (keyCodeToButtonId[event.keyCode] === undefined || document.getElementById(keyCodeToButtonId[event.keyCode]).disabled) {
                    return;
                }

                // check for special chars
                if (keyCodeToButtonId[event.keyCode] === "enqueue") {
                    calculate.enqueueCalculation();
                } else if (keyCodeToButtonId[event.keyCode] === "equals") {
                    calculate.calculate();
                } else {
                    // add char to calculation
                    calculation.value += String.fromCharCode(event.keyCode);
                }

                // update buttons
                buttons.setButtonsEnabled();
            };
        }());

    clearInput = function () {
        // clears the current calculation
        calculate.clearQueue();
        
        calculation.value = "";

        buttons.setButtonsEnabled();
    };
    buttons = (function () {
        var getLastChar = function () {
                return calculation.value[calculation.value.length - 1];
            },
            isLastCharSign = function () {
                return isNaN(getLastChar());
            },
            getLeftBracketCount = function () {
                // using regular expression to get count of brackets
                return (calculation.value.match(/\(/g) || []).length;
            },
            getRightBracketCount = function () {
                // using regular expression to get count of brackets
                return (calculation.value.match(/\)/g) || []).length;
            },
            isValidCalculation = function () {
                // calculation is valid if the last char is a number or right bracket and the brackets match and at least one operator is used
                return (!isLastCharSign() || getLastChar() === ")") &&
                    getLeftBracketCount() === getRightBracketCount() &&
                    calculation.value.search(/\+|\-|\*|\//) > -1;
            },
            leftBracket = document.getElementById('leftBracket'),
            rightBracket = document.getElementById('rightBracket'),
            encloseBracket = document.getElementById('encloseBracket'),
            enqueue = document.getElementById('enqueue'),

            plus = document.getElementById('plus'),
            minus = document.getElementById('minus'),
            times = document.getElementById('times'),
            division = document.getElementById('division'),
            equals = document.getElementById('equals'),

            one = document.getElementById('one'),
            two = document.getElementById('two'),
            three = document.getElementById('three'),
            four = document.getElementById('four'),
            five = document.getElementById('five'),

            six = document.getElementById('six'),
            seven = document.getElementById('seven'),
            eight = document.getElementById('eight'),
            nine = document.getElementById('nine'),
            zero = document.getElementById('zero'),

            remove = document.getElementById('remove'),
            clear = document.getElementById('clear');

        return {
            setButtonsEnabled: function () {
                var signsEnabled = (!isLastCharSign() || getLastChar() === ")") && calculation.value.indexOf("=") < 0,
                    zeroEnabled = getLastChar() !== "/",
                    leftBracketEnabled = (isLastCharSign() && getLastChar() !== ")") || calculation.value.indexOf("=") > 0,
                    rightBracketEnabled = getLeftBracketCount() > getRightBracketCount() && calculation.value.indexOf("=") < 0 && getLastChar() !== "(" && !isLastCharSign(),
                    validCalculation = isValidCalculation(), // no need to call this function more than once...
                    numbersEnabled = getLastChar() !== ")";

                leftBracket.disabled = !leftBracketEnabled;
                rightBracket.disabled = !rightBracketEnabled;
                encloseBracket.disabled = !validCalculation || calculation.value.indexOf("=") > 0;
                enqueue.disabled = !validCalculation || calculation.value.indexOf("=") > 0;

                plus.disabled = !signsEnabled;
                minus.disabled = !signsEnabled;
                times.disabled = !signsEnabled;
                division.disabled = !signsEnabled;
                equals.disabled = !validCalculation || calculation.value.indexOf("=") > 0;

                one.disabled = !numbersEnabled;
                two.disabled = !numbersEnabled;
                three.disabled = !numbersEnabled;
                four.disabled = !numbersEnabled;
                five.disabled = !numbersEnabled;

                six.disabled = !numbersEnabled;
                seven.disabled = !numbersEnabled;
                eight.disabled = !numbersEnabled;
                nine.disabled = !numbersEnabled;
                zero.disabled = !numbersEnabled || !zeroEnabled;

                remove.disabled = calculation.value.length === 0;
                clear.disabled = calculation.value.length === 0;
            },
            addButtonEvents: function () {
                leftBracket.addEventListener("click", function () {
                    buttonClick(leftBracket);
                });
                rightBracket.addEventListener("click", function () {
                    buttonClick(rightBracket);
                });
                encloseBracket.addEventListener("click", encloseBrackets);
                enqueue.addEventListener("click", calculate.enqueueCalculation);

                plus.addEventListener("click", function () {
                    buttonClick(plus);
                });
                minus.addEventListener("click", function () {
                    buttonClick(minus);
                });
                times.addEventListener("click", function () {
                    buttonClick(times);
                });
                division.addEventListener("click", function () {
                    buttonClick(division);
                });
                equals.addEventListener("click", function () {
                    buttonClick(equals);
                });

                one.addEventListener("click", function () {
                    buttonClick(one);
                });
                two.addEventListener("click", function () {
                    buttonClick(two);
                });
                three.addEventListener("click", function () {
                    buttonClick(three);
                });
                four.addEventListener("click", function () {
                    buttonClick(four);
                });
                five.addEventListener("click", function () {
                    buttonClick(five);
                });

                six.addEventListener("click", function () {
                    buttonClick(six);
                });
                seven.addEventListener("click", function () {
                    buttonClick(seven);
                });
                eight.addEventListener("click", function () {
                    buttonClick(eight);
                });
                nine.addEventListener("click", function () {
                    buttonClick(nine);
                });
                zero.addEventListener("click", function () {
                    buttonClick(zero);
                });

                remove.addEventListener("click", removeChar);
                clear.addEventListener("click", clearInput);
            }
        };
    }());
    
    ///////////////////////////
    // BUTTON INITIALIZATION //
    ///////////////////////////

    // add all "normal" button events
    buttons.addButtonEvents();

    // set button states
    buttons.setButtonsEnabled();

    /////////////////////
    // EVENT LISTENERS //
    /////////////////////
    // note: no "onkeyXXX"-properties were overridden
    // event listeners were added instead
    // (several different ones had to be used...)

    // add keydown event listener (for special keys)
    window.addEventListener("keydown", function () {
        // backspace events
        if (event.keyCode === 8) {
            // remove the last char and set buttons accordingly
            removeChar();
            // cancel original event since we already removed the character
            event.preventDefault();
        }

        // escape events
        if (event.keyCode === 27) {
            clearInput();
            // cancel original event since we already did what we intended
            event.preventDefault();
        }

        // prevent left and up arrow
        if (event.keyCode === 37 || event.keyCode === 38) {
            event.preventDefault();
        }
    });

    // use keypress to get "evaluated" key (with potential shift-modifier)
    window.addEventListener("keypress", processKeyPress);

    // ensure cursor is always at the end when input box is clicked
    calculation.addEventListener("click", function (e) {
        setCursor();
    });
}());